package main

import (
	"os"
	"testing"
)

func TestSendTwilioSMS(t *testing.T) {
	os.Setenv("TWILIO_NUMBER_TO", "33")

	numberFrom := os.Getenv("TWILIO_NUMBER_FROM")
	if numberFrom == "" {
		os.Setenv("TWILIO_NUMBER_FROM", "33")
	}

	_, err := SendTwilioSMS()

	if err != nil {
		t.Error("Failed to send SMS: ", err)
	}
}
