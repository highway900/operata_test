# operata_test

Coding test for operata

Operata Task
============

### User Story
'As an Admin user of a PTV account, I want to receive an SMS whenever a new log file is successfully uploaded in the S3 bucket.'

### Notes:
1. Your solution should be run on a cloud platform.
2. You have the freedom to use any suitable programming languages/tools for this exercise.
3. Please utilise Twilio SMS API to send the SMS.

#### Task B: Explain your solution:
1. Design decisions / Assumptions
2. Testing strategies
3. Deployment / Automation strategies
4. Operational Supportability considerations
5. Issues encountered

Task C: add code to a repo of your choice using a formatted readme.md to explain each point in task B

# Installation

Checkout the [repo](gitlab.com/highway900/operata_test)

Test with `TWILIO_ACCOUNT_SID=ACXXXXXXXXXXXXXXXXXX TWILIO_AUTH_TOKEN=1bXXXXXXXXXXXXX TWILIO_NUMBER_FROM=+614XXXXXXX go test` this will send a message to the number `33` which is a twilio test number.

You will require your the env variables to be set for the test to work locally.

# Solution

### Design Decisions / Assumptions

The task itself is simple enough to create a working solution, working being the key word here. Without expanding the scope of the task to much I have decided to focus more on a working example and the automation side of the task as this is where in my opinion the bulk of the work will be.

Using gitlab pipelines I have setup a basic CI system to test and push the resulting artifact to AWS.
The pipline uses a docker container to build the artifact that is deployed to the S3 bucket for execution, this container **can** be used for local development (but is not for this demo).

- Deployment will be simple as possible
- Not done
    + IAM roles for AWS as this is extended scope (partially true)
    + Extensive testing e.g. using twilio feedback api
    + Controlling of TO/FROM numbers of twilio API
        * These are HARD baked into environment variables
        * ideally configured from a db
    + SID is also baked as env variables
    + Full SAM template deployment
    + Use of event information from the upload to trigger a more specific message (e.g. filenam.x has been uploaded)
    + nice logging
- Testing is currently manual due to time constaints

### Testing strategies

Testing is currently (unfortunately) manual. The ideal situation would be to create events that simulate a S3 event that then triggers the handler (sends SMS) this could then me validated using the twilio feedback api or a webhook.
An integration test could easily be written to to upload a file to S3 and check the feedback

The challenge here is that the result requires a phone to really test 100% correctly. I believe the feedback would provide a better result for testing the message is sent. More investigation would be required.

For testing locally with `go test` you are required to set the env vars per the Install instruction above.

The Gitlab pipeline is setup to run the tests and also check test coverage.

### Deployment / Automation strategies

Using Gitlab pipelines the application automatically tests and then deploys (given the tests pass) the application to AWS. The pipelines config is stored in the repo and is the type of configuration that is locked in and reused (e.g. doesn't change much)

Deployment simply uses the aws cli that comes with the container to copy the artifact to a S3 bucket that the lambda function uses to run the code.

Access to the S3 bucket is managed from the gitlab project with protected keys generated for a deployment IAM user.

Further automation specifically using code as configuration for creating the lambda function + S3 bucket should be done through SAM templates, this was partially done but parked due to time contraints.

### Operational Supportability considerations

Operational support would be lean with respect to running applications on serverless infrastructure. In the case of allowing for flexibility in provider (e.g. aws, gcp, etc) a agnostic wrapper such as [serverless framework](https://serverless.com) might be used to not fix to a single provider which could be handy for say moving based on cost.
With that said automation would incur overhead switching the deployment configuration around, most of this time would be testing.

As I understand there is no SLA for bucket triggers. A scheduled event could be a solution to this instead. This requires more investigation.

### Issues encountered

- The lambda trigger while testing continually sent SMSs at one point.
    + this was likely the first bucket I initially tested on which was not a brand new one.
- Automated testing required a decent working knowledge of multiple configurations and APIs
- API keys for twilio are really for client web apps. This was confusing at first, but I am sure would be clearer with more experience with the API.
- AWS SAM templates are a bohemoth my working knowledge is rusty as this is something that is not regurally created.

### Tasks (rough breakdown of user story sub tasks)

- Setup lambda, **write** an example task (investigation)
- Setup S3
- S3 event to tigger lambda function on upload
- Lock it all down (no access from outside)
- Automate deployment via Gitlab
    + deployment keys in gitlab
- Write instructions
- Potentially use serverless framework
- Write tests
    + investigate lambda testing
