package main

import (
	"context"
	"errors"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

// Response is a message wrapper for aws lambda
type Response struct {
	Message string `json:"message"`
	Ok      bool   `json:"ok"`
}

// SendTwilioSMS sends an SMS to the number defined in the env variable TWILIO_NUMBER_TO
func SendTwilioSMS() (*http.Response, error) {
	numberTo := os.Getenv("TWILIO_NUMBER_TO")
	numberFrom := os.Getenv("TWILIO_NUMBER_FROM")

	accountSid := os.Getenv("TWILIO_ACCOUNT_SID")
	authToken := os.Getenv("TWILIO_AUTH_TOKEN")

	if numberFrom == "" || numberTo == "" || accountSid == "" || authToken == "" {
		errMsg := fmt.Sprintf(`Invalid environment variables for twilio configuration: \n
			numberTo: %s, numberFrom: %s | check authToken and accountSid`, numberTo, numberFrom)
		return nil, errors.New(errMsg)
	}

	urlStr := fmt.Sprintf("https://api.twilio.com/2010-04-01/Accounts/%s/Messages.json", accountSid)

	msgData := url.Values{}
	msgData.Set("To", numberTo)
	msgData.Set("From", numberFrom)
	msgData.Set("Body", "New logs uploaded")
	msgDataReader := *strings.NewReader(msgData.Encode())

	// Create HTTP request client
	var client = &http.Client{
		Timeout: time.Second * 10,
	}
	req, _ := http.NewRequest("POST", urlStr, &msgDataReader)
	req.SetBasicAuth(accountSid, authToken)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(req)
	return resp, err
}

// HandleRequest returns a string
func HandleRequest(ctx context.Context, event events.S3Event) (Response, error) {
	fmt.Println(ctx)

	fmt.Println(event)

	resp, err := SendTwilioSMS()

	return Response{
		Message: fmt.Sprintf("Processed event %d", resp.StatusCode),
		Ok:      true,
	}, err
}

func main() {
	lambda.Start(HandleRequest)
}
